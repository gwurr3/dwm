/* See LICENSE file for copyright and license details. */

/* appearance */
static const char font[]            = "-glenn-Terminus5-Medium-R-Normal--12-120-72-72-C-60-ISO8859-1";
#include "colors.h"
#define NUMCOLORS         8
static const char colors[MAXCOLORS][ColLast][8] = {
		/* border     fg         bg       */
		{ B_base02, B_base05, B_base00 }, /* 0 = normal */
		{ B_base08, B_base08, B_base00 }, /* 1 = selected */
		{ B_base0E, B_base06, B_base0E }, /* 2 = urgent */
		{ B_base00, B_base0B, B_base00 }, /* 3 = green */
		{ B_base00, B_base0A, B_base00 }, /* 4 = yellow */
		{ B_base00, B_base0C, B_base00 }, /* 5 = cyan */
		{ B_base00, B_base0E, B_base00 }, /* 6 = magenta */
		{ B_base00, B_base08, B_base00 }, /* 7 = orange */
};
static const unsigned int borderpx  = 4;        /* border pixel of windows */
static const unsigned int gappx     = 4;       /* gap pixel between windows */
/* 
 * TODO: make a function to inc/dec gap size during runtime. 
 * http://dwm.suckless.org/patches/dwm-dualstatus-6.0.diff 
 * ^ has a good example of adding a simple hotkey function
 */
static const unsigned int snap      = 8;        /* snap pixel */
static const Bool showbar           = True;     /* False means no bar */
static const Bool topbar            = True;     /* False means bottom bar */
#define BOTTOMGAP 15 // the size of lemonbar on the bottom

/* tagging --------------------1 -- -2- ---3   ----4--   --5--   --6-  --7----8----9*/
static const char *tags[] = { "Â ", "Â ", "Ò ", "Ò ",   "  ", "È ", "È ", "À ", "À" };

static const Rule rules[] = {
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       1 << 5,            True,        -1 },
	{ "Firefox",  NULL,       NULL,       1 << 7,       False,       0 },
	{ "google-chrome",  NULL,       NULL,       1 << 8,       False,       0 },
	{ "chromium",  NULL,       NULL,       1 << 8,       False,       0 },
	{ "surf",  NULL,       NULL,       1 << 8,       False,       0 },
	{ "luakit",  NULL,       NULL,       1 << 8,       False,       0 },
	{ "Pidgin",  NULL,       NULL,       1 << 2,       False,       1 },
	{ "Hexchat",  NULL,       NULL,       1 << 2,       False,       1 },
	{ "Wine",  NULL,       NULL,       1 << 4,       False,       -1 },
	{ "KayakoDesktop.exe",  NULL,       NULL,       1 << 4,       False,       -1 },
	{ "Slack",  NULL,       NULL,       1 << 3,       False,       1 },
	{ "MPlayer",  NULL,       NULL,       1 << 6,       True,       -1 },
	{  NULL,          NULL,      "scratchpad", 0,            True,        -1 },
	{  "Autokey-qt",          NULL,	NULL, 0,            True,        -1 },
	{  "com-sun-javaws-Main",          NULL,	NULL, 0,            True,        -1 },
	{  "Steam.exe",          NULL,	NULL, 0,            True,        -1 },
};

/* layout(s) */
static const float mfact      = 0.65; /* factor of master area size [0.05..0.95] */
static const int nmaster      = 1;    /* number of clients in master area */
static const Bool resizehints = False; /* True means respect size hints in tiled resizals */

#include "bstack.c"
#include "bstackhoriz.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "",      tile },    /* first entry is default */
	{ "",      NULL },    /* no layout function means floating behavior */
	{ "",      monocle },
	{ "",      bstack },
	{ "",      bstackhoriz },
};

/* key definitions */
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define CTRKEY ControlMask
#define TAGKEYS(KEY,TAG) \
	{ KeyPress,   MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ KeyPress,   MODKEY|Mod1Mask,              KEY,      comboview,      {.ui = 1 << TAG} }, \
	{ KeyPress,   MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ KeyPress,   MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ KeyPress,   MODKEY|Mod1Mask|ShiftMask,    KEY,      combotag,       {.ui = 1 << TAG} }, \
	{ KeyPress,   MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
/*static const char *dmenucmd[] = { "dmenu_run", "-fn", font, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbgcolor, "-sf", selfgcolor, NULL }; */
static const char *dmenucmd[]      = { "dmenu_run", "-i", "-p", "Run command:", "-fn", font, "-nb", colors[0][ColBG], "-nf", colors[0][ColFG],"-sb", colors[1][ColBG], "-sf", colors[1][ColFG], NULL };
static const char *dmenu2cmd[]      = { "dmenu2_run", "-i", "-p", "Run command:", "-fn", font, "-nb", colors[0][ColBG], "-nf", colors[0][ColFG],"-sb", colors[1][ColBG], "-sf", colors[1][ColFG], NULL };
static const char *termcmd[]  = { "run_terminal", NULL };
static const char *screenshot[]  = {"take_screenshot", NULL};
static const char *screenshot_upload[]  = {"take_screenshot_upload", NULL};
static const char *scratchpadcmd[] = { "run_scratchpad",  NULL };

static Key keys[] = {
	/* type       modifier                      key        function        argument */
	{ KeyPress,   MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ KeyPress,   MODKEY|ShiftMask,             XK_p,      spawn,          {.v = dmenu2cmd } },
	{ KeyPress,   MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ KeyRelease, 0,               		    XK_Print,  spawn,          {.v = screenshot } },
	{ KeyRelease, MODKEY,               	    XK_Print,  spawn,          {.v = screenshot_upload } },
	{ KeyPress,   MODKEY|ShiftMask,	    	    XK_s,      spawn,	       {.v = scratchpadcmd } },
	{ KeyPress,   MODKEY,                       XK_b,      togglebar,      {0} },
	{ KeyPress,   MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ KeyPress,   MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ KeyPress,   MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ KeyPress,   MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ KeyPress,   MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ KeyPress,   MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ KeyPress,   MODKEY,                       XK_Return, zoom,           {0} },
	{ KeyPress,   MODKEY,                       XK_Tab,    view,           {0} },
	{ KeyPress,   MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ KeyPress,   MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ KeyPress,   MODKEY|ShiftMask,             XK_t,      setlayout,      {.v = &layouts[3]} },
	{ KeyPress,   MODKEY|ShiftMask|Mod1Mask,    XK_t,      setlayout,      {.v = &layouts[4]} },
	{ KeyPress,   MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ KeyPress,   MODKEY|ShiftMask,             XK_f,      setlayout,      {.v = &layouts[2]} },
	{ KeyPress,   MODKEY,                       XK_space,  setlayout,      {0} },
	{ KeyPress,   MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ KeyPress,   MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ KeyPress,   MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ KeyPress,   MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ KeyPress,   MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ KeyPress,   MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ KeyPress,   MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
        { KeyPress,   ALTKEY,                       XK_j,      moveresize,     {.v =  "0x 25y 0w 0h"} },
        { KeyPress,   ALTKEY,                       XK_k,      moveresize,     {.v =  "0x -25y 0w 0h"} },
        { KeyPress,   ALTKEY,                       XK_l,      moveresize,     {.v =  "25x 0y 0w 0h"} },
        { KeyPress,   ALTKEY,                       XK_h,      moveresize,     {.v =  "-25x 0y 0w 0h"} },
        { KeyPress,   ALTKEY|CTRKEY,                XK_j,      moveresize,     {.v =  "0x 0y 0w 25h"} },
        { KeyPress,   ALTKEY|CTRKEY,                XK_k,      moveresize,     {.v =  "0x 0y 0w -25h"} },
        { KeyPress,   ALTKEY|CTRKEY,                XK_l,      moveresize,     {.v =  "0x 0y 25w 0h"} },
        { KeyPress,   ALTKEY|CTRKEY,                XK_h,      moveresize,     {.v =  "0x 0y -25w 0h"} },
        { KeyPress,   ALTKEY|ShiftMask,             XK_j,      moveresize,     {.v =  "0x 50y 0w 0h"} },
        { KeyPress,   ALTKEY|ShiftMask,             XK_k,      moveresize,     {.v =  "0x -50y 0w 0h"} },
        { KeyPress,   ALTKEY|ShiftMask,             XK_l,      moveresize,     {.v =  "50x 0y 0w 0h"} },
        { KeyPress,   ALTKEY|ShiftMask,             XK_h,      moveresize,     {.v =  "-50x 0y 0w 0h"} },
        { KeyPress,   ALTKEY|ShiftMask|CTRKEY,      XK_j,      moveresize,     {.v =  "0x 0y 0w 50h"} },
        { KeyPress,   ALTKEY|ShiftMask|CTRKEY,      XK_k,      moveresize,     {.v =  "0x 0y 0w -50h"} },
        { KeyPress,   ALTKEY|ShiftMask|CTRKEY,      XK_l,      moveresize,     {.v =  "0x 0y 50w 0h"} },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ KeyPress,   MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	        /* click                event mask      button          function        argument */
	        { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	        { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	        { ClkWinTitle,          0,              Button2,        zoom,           {0} },
	        { ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	        { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	        { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	        { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	        { ClkTagBar,            0,              Button1,        view,           {0} },
	        { ClkTagBar,            0,              Button3,        toggleview,     {0} },
	        { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	        { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

