// base16 greenscreen colors https://chriskempson.github.io/base16/#greenscreen
#define G_base00	"#001100"
#define G_base01	"#003300"
#define G_base02	"#005500"
#define G_base03	"#007700"
#define G_base04	"#009900"
#define G_base05	"#00bb00"
#define G_base06	"#00dd00"
#define G_base07	"#00ff00"
#define G_base08	"#007700"
#define G_base09	"#009900"
#define G_base0A	"#007700"
#define G_base0B	"#00bb00"
#define G_base0C	"#005500"
#define G_base0D	"#009900"
#define G_base0E	"#00bb00"
#define G_base0F	"#005500"



// dark solarized colors
#define S_base03        "#002b36"
#define S_base02        "#073642"
#define S_base01        "#586e75"
#define S_base00        "#657b83"
#define S_base0         "#839496"
#define S_base1         "#93a1a1"
#define S_base2         "#eee8d5"
#define S_base3         "#fdf6e3"



#define S_yellow        "#b58900"
#define S_orange        "#cb4b16"
#define S_red           "#dc322f"
#define S_magenta       "#d33682"
#define S_violet        "#6c71c4"
#define S_blue          "#268bd2"
#define S_cyan          "#2aa198"
#define S_green         "#859900"

// Base16 Eighties Chris Kempson (http://chriskempson.com)

#define B_base00	"#2d2d2d"
#define B_base01	"#393939"
#define B_base02	"#515151"
#define B_base03	"#747369"
#define B_base04	"#a09f93"
#define B_base05	"#d3d0c8"
#define B_base06	"#e8e6df"
#define B_base07	"#f2f0ec"
#define B_base08	"#f2777a"
#define B_base09	"#f99157"
#define B_base0A	"#ffcc66"
#define B_base0B	"#99cc99"
#define B_base0C	"#66cccc"
#define B_base0D	"#6699cc"
#define B_base0E	"#cc99cc"
#define B_base0F	"#d27b53"


