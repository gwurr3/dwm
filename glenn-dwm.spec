Name:		dwm
Version:	6.0
Release:	711%{?dist}
Summary:	my preferred build of dwm

Group:		glenns
License:	MIT
Source0:	https://bitbucket.org/gwurr3/dwm/get/master.tar.gz
BuildRoot:	%{buildroot}

BuildRequires:	gcc make libX11-devel libXinerama-devel
Requires:	xorg-x11-server-utils fontconfig dmenu xorg-x11-font-utils libXinerama xorg-x11-server-common


%description
my preferred build of the best window manager
in the existance of the universe.
this includes a patched font file, as well!

%prep
%setup -q


%build
make %{?_smp_mflags} X11INC=/usr/include/X11 PREFIX=/usr


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} PREFIX=/usr
mkdir -p %{buildroot}/usr/share/fonts/glenndwm/
cp glenn-terminus5-112n.bdf %{buildroot}/usr/share/fonts/glenndwm/glenn-terminus5-112n.bdf

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%verify(mode md5 size mtime) /usr/bin/dwm
%verify(mode md5 size mtime) /usr/share/fonts/glenndwm/glenn-terminus5-112n.bdf
%verify(mode md5 size mtime) /usr/share/man/*
%doc

%post
mkfontdir /usr/share/fonts/glenndwm/
fc-cache /usr/share/fonts
%postun
rm -rf /usr/share/fonts/glenndwm/
fc-cache /usr/share/fonts

%changelog
